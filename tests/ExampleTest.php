<?php

namespace MultitechOsp\LaravelAwsSecretsManager\Tests;

use MultitechOsp\LaravelAwsSecretsManager\LaravelAwsSecretsManager;
use MultitechOsp\LaravelAwsSecretsManager\LaravelAwsSecretsManagerServiceProvider;
use Orchestra\Testbench\TestCase;

class ExampleTest extends TestCase
{
    protected function getPackageProviders($app)
    {
        return [LaravelAwsSecretsManagerServiceProvider::class];
    }

    /** @test */
    public function true_is_true()
    {
        (new LaravelAwsSecretsManager())->loadSecrets();
        $this->assertTrue(true);
    }
}
