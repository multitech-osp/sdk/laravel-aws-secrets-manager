<?php

return [

    /*
    |--------------------------------------------------------------------------
    | AWS Region where secrets are stored
    |--------------------------------------------------------------------------
    |
    | The AWS Region where secrets are stored.
    |
    */
    'region' => env('AWS_SECRETS_MANAGER_DEFAULT_REGION', 'us-east-1'),

    /*
    |--------------------------------------------------------------------------
    | AWS Version where secrets are stored
    |--------------------------------------------------------------------------
    |
    | The AWS Version where secrets are stored.
    |
    */
    'version' => env('AWS_SECRETS_MANAGER_VERSION', '2017-10-17'),

    /*
    |--------------------------------------------------------------------------
    | Secrets Manager Application Name
    |--------------------------------------------------------------------------
    |
    | Secrets Manager Application Name
    | Eg.: '/squad-name/app-name'
    |
    */
    'name' => env('AWS_SECRETS_MANAGER_NAME', ''),

    /*
    |--------------------------------------------------------------------------
    | AWS Secrets Manager enabled
    |--------------------------------------------------------------------------
    |
    | AWS Secrets Manager enabled
    |
    */
    'enabled' => env('AWS_SECRETS_MANAGER_ENABLED', true),

    /*
    |--------------------------------------------------------------------------
    | Cache driver
    |--------------------------------------------------------------------------
    |
    | Here you should chose how is your cache driver
    | I Recommend "File" option
    |
    */
    'cache' => [
        'driver' => env('AWS_SECRETS_MANAGER_CACHE_DRIVER', 'file')
    ],

];
