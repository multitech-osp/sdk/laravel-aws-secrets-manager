<?php

namespace MultitechOsp\LaravelAwsSecretsManager;

use Illuminate\Support\ServiceProvider;

class LaravelAwsSecretsManagerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__.'/../config/config.php' => config_path('aws-secrets-manager.php'),
            ], 'config');
        }

        if (config('aws-secrets-manager.enabled', true)) {
            (new LaravelAwsSecretsManager())->loadSecrets();
        }
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        // Automatically apply the package configuration
        $this->mergeConfigFrom(__DIR__.'/../config/config.php', 'aws-secrets-manager');

        // Register the main class to use with the facade
        $this->app->singleton('aws-secrets-manager', function () {
            return new LaravelAwsSecretsManager();
        });
    }
}
