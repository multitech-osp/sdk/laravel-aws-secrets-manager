<?php

namespace MultitechOsp\LaravelAwsSecretsManager;

use Aws\SecretsManager\SecretsManagerClient;
use Illuminate\Support\Facades\Cache;

class LaravelAwsSecretsManager
{
    /**
     * Name
     *
     * @var string
     */
    private $name;

    /**
     * Construct function
     */
    public function __construct()
    {
        $this->name = config('aws-secrets-manager.name');
        $this->cache = Cache::store(config('aws-secrets-manager.cache.driver', 'file'));
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function loadSecrets()
    {
        if (!$this->cache->has('aws-secrets-manager')) {
            $secretValues = $this->getSecretVariables();
            $this->cache->forever('aws-secrets-manager', $secretValues);
        }

        $secretValues = $this->cache->get('aws-secrets-manager');
        foreach ($secretValues as $key => $value) {
            putenv("$key=$value");
        }
    }

    /**
     * Get Secret Variables
     *
     * @return array
     */
    private function getSecretVariables(): array
    {
        try {
            $client = new SecretsManagerClient([
                'version' => config('aws-secrets-manager.version', '2017-10-17'),
                'region' => config('aws-secrets-manager.region', 'us-east-1'),
            ]);

            $resultSecretValue = $client->getSecretValue([
                'SecretId' => $this->name,
            ]);

            if (isset($resultSecretValue['SecretString'])) {
                return json_decode($resultSecretValue['SecretString'], true);
            }

            return [];
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
