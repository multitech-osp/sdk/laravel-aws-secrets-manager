<?php

namespace MultitechOsp\LaravelAwsSecretsManager;

use Illuminate\Support\Facades\Facade;

/**
 * @see \MultitechOsp\LaravelAwsSecretsManager\Skeleton\SkeletonClass
 */
class LaravelAwsSecretsManagerFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'aws-secrets-manager';
    }
}
