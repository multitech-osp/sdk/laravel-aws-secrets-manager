# Changelog

All notable changes to `laravel-aws-secrets-manager` will be documented in this file

## 1.0.0 - initial release

- initial release

## 1.0.1 - test auto publish package

- test auto publish package with gitlab

## 1.0.2 - docs readme

- update readme with AWS secrets manager api docs

