# AWS Secrets Manager

Manage environment secrets using AWS Secrets Manager.

## Installation

You can install the package via composer:

```bash
composer require multitech-osp/laravel-aws-secrets-manager
```

Publish Config:
```
php artisan vendor:publish --provider="MultitechOsp\LaravelAwsSecretsManager\LaravelAwsSecretsManagerServiceProvider"
```

After publish `aws-secrets-manager` in `config` path, just change the config for your app.

## Docs

AWS Api for secret manager -> https://docs.aws.amazon.com/aws-sdk-php/v3/api/api-secretsmanager-2017-10-17.html

## Todo

- [ ] Make all tests 

